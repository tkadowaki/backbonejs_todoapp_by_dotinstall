(function() {
  var Task, TaskAddView, TaskView, Tasks, TasksView, taskAddView, tasks, tasksView;
  Task = Backbone.Model.extend({
    defaults: {
      title: "do somthing",
      completed: false
    },
    validate: function(attr) {
      if (_.isEmpty(attr.title)) {
        return 'title must not be empty!';
      }
    },
    initialize: function() {
      return this.on('invalid', function(model, error) {
        return $('#error').html(error);
      });
    }
  });
  Tasks = Backbone.Collection.extend({
    model: Task
  });
  TaskView = Backbone.View.extend({
    tagName: 'li',
    initialize: function() {
      this.model.on('destroy', this.remove, this);
      return this.model.on('change', this.render, this);
    },
    events: {
      'click .delete': 'destroy',
      'click .toggle': 'toggle'
    },
    toggle: function() {
      return this.model.set('completed', !this.model.get('completed'));
    },
    destroy: function() {
      if (confirm('are you sure')) {
        return this.model.destroy();
      }
    },
    remove: function() {
      return this.$el.remove();
    },
    template: _.template($('#task-template').html()),
    render: function() {
      var template;
      template = this.template(this.model.toJSON());
      this.$el.html(template);
      return this;
    }
  });
  TasksView = Backbone.View.extend({
    tagName: 'ul',
    initialize: function() {
      this.collection.on('add', this.addNew, this);
      this.collection.on('change', this.updateCount, this);
      return this.collection.on('destroy', this.updateCount, this);
    },
    addNew: function(task) {
      var taskView;
      taskView = new TaskView({
        model: task
      });
      this.$el.append(taskView.render().el);
      $('#title').val('').focus();
      return this.updateCount();
    },
    updateCount: function() {
      var uncompletedTasks;
      uncompletedTasks = this.collection.filter(function(task) {
        return !task.get('completed');
      });
      return $('#count').html(uncompletedTasks.length);
    },
    render: function() {
      this.collection.each(function(task) {
        var taskView;
        taskView = new TaskView({
          model: task
        });
        this.$el.append(taskView.render().el);
        return this.updateCount();
      }, this);
      return this;
    }
  });
  TaskAddView = Backbone.View.extend({
    el: "#addTask",
    events: {
      'submit': 'submit'
    },
    submit: function(e) {
      var task, title;
      e.preventDefault();
      title = $('#title').val();
      task = new Task();
      if (task.set({
        title: title
      }, {
        validate: true
      })) {
        this.collection.add(task);
        return $('#error').empty();
      }
    }
  });
  tasks = new Tasks([
    {
      title: 'task1',
      completed: true
    }, {
      title: 'task2'
    }, {
      title: 'task3'
    }
  ]);
  tasksView = new TasksView({
    collection: tasks
  });
  taskAddView = new TaskAddView({
    collection: tasks
  });
  $("#tasks").append(tasksView.render().el);
  return this;
})();
