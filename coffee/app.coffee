do() ->

  # Model
  Task = Backbone.Model.extend(
    defaults:
      title: "do somthing"
      completed: false
    # モデルのバリデーションを行うメソッド
    validate: (attr) ->
      if _.isEmpty attr.title then return 'title must not be empty!'
    # タスクモデルの初期化処理
    initialize: ->
      # バリデーションが失敗した際のイベントを監視する
      @.on 'invalid' , ( model , error ) ->
        $('#error').html(error)


  )
  # Collection
  Tasks = Backbone.Collection.extend( model:Task )

  # 1タスクを表示するビュー
  TaskView = Backbone.View.extend(
    tagName: 'li'
    # ビューの初期化処理
    initialize: ->
      # インスタンス内の破棄メソッドの発火を監視する
      @model.on 'destroy' , @remove , @
      # インスタンス内のモデル値の変更を監視する
      # 変更があった場合は再描画する
      @model.on 'change' , @render , @
    # 監視するDOMイベント
    events:
      'click .delete': 'destroy'
      'click .toggle': 'toggle'
    # タスクのチェック状態を切り替えるメソッド
    toggle: ->
      @model.set 'completed' , not @model.get 'completed'
    # モデルを破棄するメソッド
    destroy: ->
      if confirm('are you sure')
        @model.destroy()
    # モデルを表示しているDOMを破棄するメソッド
    remove: ->
      @$el.remove()
    # ビューが使用するテンプレート
    template: _.template $('#task-template').html()
    # ビューの描画処理を行うメソッド
    render: ->
      template = @template @model.toJSON()
      # インスタンス内のエレメント要素にHTMLとして追加
      @$el.html template
      # 自信のインスタンスを返却する
      return @
  )

  # タスクリストを表示するビュー
  TasksView = Backbone.View.extend(
    tagName: 'ul'
    # ビューの初期化処理
    initialize: ->
      # コレクションの追加を監視
      @collection.on 'add' , @addNew , @
      # コレクションの変更を監視
      @collection.on 'change' , @updateCount , @
      # コレクションの破棄を監視
      @collection.on 'destroy' , @updateCount , @
    # タスクビューを新しく追加するメソッド
    addNew: (task) ->
      taskView = new TaskView( model: task )
      @$el.append(taskView.render().el)
      # あたらしく追加されたタスクにフォーカスを当てる
      $('#title').val('').focus()
      @updateCount()
    # 未終了のタスクをカウントするメソッド
    updateCount: ->
      # フィルターをかけて未終了のコレクションのみ取得
      uncompletedTasks = @collection.filter((task)->
        return not task.get 'completed'
        )
      # DOMに取得できたコレクションの数を挿入
      $('#count').html(uncompletedTasks.length)

    render: ->
      @collection.each (task) ->
        taskView = new TaskView( model: task )
        @$el.append taskView.render().el
        # タスクのカウントを表示
        @updateCount()
      , @
      return @

  )

  # タスクを表示するビュー
  TaskAddView = Backbone.View.extend(
    el: "#addTask"
    events:
      # FORMの送信イベントの監視
      'submit': 'submit'
    # FORM送信時に呼び出されるメソッド
    submit: (e) ->
      e.preventDefault()
      title = $('#title').val()
      #task = new Task( title:title )
      # バリデーションを効かせるためタスクをセットする
      task = new Task()
      # セットされたタスクタイトルに問題がなければコレクションに追加
      if task.set( {title:title} , {validate:true} )
        @collection.add task
        # エラーメッセージを初期化
        $('#error').empty()


  )

  tasks = new Tasks([
    {
      title: 'task1'
      completed: true
    }
    { title: 'task2' }
    { title: 'task3' }
  ])


  # タスクリスト表示用のビューを生成
  tasksView = new TasksView collection: tasks
  # タスク追加用のビューを生成
  taskAddView = new TaskAddView collection: tasks
  # タスクリストの表示
  $("#tasks").append(tasksView.render().el)

  return @
