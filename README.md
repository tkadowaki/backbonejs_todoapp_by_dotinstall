# 概要

ドットインストールの Backbone.js の講座を CoffeeScript で組んだリポジトリ.

# 開発環境のセットアップ

開発環境は Windows とする.

## Chocolatey のインストール

Chocolatey は Windows 専用のパッケージ管理システムである.  
インストール方法は以下の URL を参照.

http://chocolatey.org/

## node.js のインストール

上記パッケージ管理システムをインストールできたら以下のコマンドをターミナルにて実行する.  
いずれも管理者権限で実行すること.

    choco install nodejs

node.js はサーバーサイド Javascipt である. node.js の機能を使用して開発環境を整えていく.
node.js について詳しくは以下の URL を参照.

http://nodejs.jp/

続いて以下のコマンドを実行する

    choco install npm

npm は node.js で作成されたアプリケーションのパッケージ管理システムである.

## gulp のインストール

npm をインストールできたら, 続けて gulp をインストールする. 以下のコマンドを実行.

    npm install gulp

gulp は node.js を使ったビルドシステムである. 詳しくは以下の URL を参照されたし.

http://gulpjs.com/

## 簡易Webサーバー takeapeek のインストール
※ この手順は他のサーバーをセットアップしていれば必須ではない.

以下のコマンドを実行.

    npm install -g takeapeek

# ビルドから実行まで

1. このリポジトリをローカルにクローンする.
2. クローンしたディレクトリ直下に移動.
3. pakage.json.default ファイルから .default を抜いてリネーム.
4. `npm install` とコマンドラインで実行.
5. 必要なパッケージがインストールされ完了したら, 続けて `gulp` と入力して実行.
6. gulp タスクが終了したら, コマンドラインで `takeapeek -p 8080` と入力して実行.
7. ブラウザを開き http://localhost:8080/index.html に遷移する.

以上.