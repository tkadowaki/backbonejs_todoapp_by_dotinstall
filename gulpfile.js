var gulp   = require('gulp');
var coffee = require('gulp-coffee');

// 捜査対象ファイルのパスを設定
var paths = {
  scripts: ['coffee/app.coffee']
};

// 変更の監視
gulp.task('watch', function() {
  gulp.watch(paths.scripts, ['coffee']);
});

// CoffeeScript のコンパイル
gulp.task('coffee', function() {
  gulp.src(paths.scripts)
    .pipe(coffee({ bare: true }))
    .pipe(gulp.dest('./js/'))
    .on('end' , function(callback){
       // 終了
      console.log('\n gulp task finished . \n')
      process.exit();
    })
});

// デフォルト処理
gulp.task('default',  ['watch', 'coffee' ] , function() {});
